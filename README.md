# Gitaly shard project repository mover

Collects a projects list in json format from specified upstream pipelines artifacts
and endeavors to scalably invoke gitlab.com repository storage move API operations.

## Setup

```bash
asdf install ruby
gem install bundler
bundle install
```

